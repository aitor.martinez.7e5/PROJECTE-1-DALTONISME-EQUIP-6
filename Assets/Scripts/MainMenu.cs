
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SceneTest()
    {
        SceneManager.LoadScene("SceneTest");
    }
    public void SceneInstruccions()
    {
        SceneManager.LoadScene("SceneInstruccions");
    }
    public void SceneMenu()
    {
        SceneManager.LoadScene("SceneMenu");
    }

    public void SceneResultats()
    {
        SceneManager.LoadScene("SceneResultats");
    }

    public void Sortir()
    {
        Application.Quit();
    } 
}
