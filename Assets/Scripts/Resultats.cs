using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = System.Random;

public class Resultats : MonoBehaviour
{
    private string _name;
    private GameObject _input;
    private string path ="Assets/Preguntes i respostes/Resultats.json";
    // Start is called before the first frame update
    void Start()
    {
        /*
        _input = GameObject.Find("GiveName");
        _name = _input.GetComponent<InputField>().text;
        Random random = new Random();
        int numero = random.Next(0, 21);
        string color = "";
        if (numero > 1) color += "Si";
        else color += "No";
        */
        //ActualitzaResultats(numero, color);
        LlegirResultats();
            
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /*
    public void ActualitzaResultats(int puntuacio, string color)
    {
        var answer = new Answer
        {
           nom = _name,
           puntuacio = puntuacio,
           daltonisme = color,
        };
       
            string json = JsonConvert.SerializeObject(answer);
            File.AppendAllText(path, json);
        
    }
    */

    public void LlegirResultats()
    {
        FileInfo file = new FileInfo(path);
        List<Answer> points = new List<Answer>();
        StreamReader sr = file.OpenText();
        string line;
        while ((line = sr.ReadLine()) != null)
        {
            Answer res = JsonConvert.DeserializeObject<Answer>(line);
            points.Add(res);
        }

        foreach (var element in points)
        {
            this.gameObject.GetComponent<Text>().text += Convert.ToString(element);
        }
    }
}

public class Answer
{
    public string nom { get; set; }
    public int puntuacio { get; set; }
    public string daltonisme { get; set; }
    public override string ToString()
    {
        return "\nL'usuari: "+nom+ " ha fallat "+puntuacio+" preguntes. "+daltonisme+" te daltonisme.";
    }
}
