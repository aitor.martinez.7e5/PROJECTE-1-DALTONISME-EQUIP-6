using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TestManager : MonoBehaviour
{
    private SpriteRenderer sr;
    private int contador = 0, contador2 =0;
    [SerializeField]
    private Sprite[] _Imagenes;
    public Button[] _buttons;
    private GameObject _text;
    private GameObject _treureimatge;
    private GameObject _tornarmenu;
    private GameObject _button1;
    private GameObject _button2;
    private GameObject _button3;
    private GameObject _respuesta1;
    private GameObject _respuesta2;
    private GameObject _respuesta3;
    private GameObject _pregunta;
    private string[] preguntesirespostes;
    private Image _image;
    private int daltonismecontador;
    
    // Start is called before the first frame update
    void Start()
    {
        

        _text.gameObject.SetActive(false);
      
       
    }
    private void Awake()
    {
        preguntesirespostes = File.ReadAllLines(Application.dataPath + "/Preguntes i respostes/Preguntas.txt");
        Debug.Log(preguntesirespostes[0]);
        sr = GetComponent<SpriteRenderer>();
        _image = GetComponent<Image>();
        _text = GameObject.Find("Final");
        Debug.Log(_text);
        _tornarmenu = GameObject.Find("backtomenu");
        _pregunta = GameObject.Find("Pregunta");
        _button1 = GameObject.Find("opcion1");
        _button2 = GameObject.Find("opcion2");
        _button3 = GameObject.Find("opcion3");
        _treureimatge = GameObject.Find("Image");
        _respuesta1 = GameObject.Find("respuesta1");
        _respuesta2 = GameObject.Find("respuesta2");
        _respuesta3 = GameObject.Find("respuesta3");
    }

    // Update is called once per frame
    void Update()
    {

    }

     public void ImageChange()
    {

        if (_Imagenes.Length == contador)
        {
            _pregunta.SetActive(false);
            _text.SetActive(true);
            _treureimatge.SetActive(false);
            _tornarmenu.SetActive(true);
           

        }
        else if (_Imagenes.Length  > contador)
        {
            contador++;
            
            _image.sprite = _Imagenes[contador];
            _pregunta.GetComponent <Text>().text = preguntesirespostes[contador2];
            _respuesta1.GetComponent <TMP_Text>().text = preguntesirespostes[contador2+1];
            _respuesta2.GetComponent<TMP_Text>().text = preguntesirespostes[contador2+2];
            _respuesta3.GetComponent<TMP_Text>().text = preguntesirespostes[contador2+3];
            contador2= contador2 + 4;
           


        }
        if (0== contador)
        {
            _button1.SetActive(false);
            _button2.SetActive(false);
            _button3.SetActive(false);
        }
        if (0 <contador)
        {

            _button1.SetActive(true);
            _button2.SetActive(true);
            _button3.SetActive(true);

        }

    }

    public void DaltonismeContador()
    {
        daltonismecontador++;
        
    }
    

}
